from django.contrib import admin

from .models import Data_MOM, Data_Company, Data_Template

# admin.site.register(Data_MOM)

class tb_data_mom(admin.ModelAdmin):
    list_display = ( 
    'id_data', 
    'judul_data',
    'meeting_title', 
    'date_time', 
    'body',
    'created_on', 
    'edited_last', 
    'deleted_on', 
    'status_document',
    'no_template')

    list_display_link = ( 
    'id_data', 
    'judul_data',
    'meeting_title', 
    'date_time', 
    'body',
    'created_on', 
    'edited_last', 
    'deleted_on', 
    'status_document',
    'no_template')

    list_per_page = 5

admin.site.register(Data_MOM, tb_data_mom)

# -------------------------------------------

class tb_data_company(admin.ModelAdmin):
    list_display = ( 
    'id_company',
    'company_name',
    'company_address',
    'logo_company')

    list_display_link = ( 
    'id_company',
    'company_name',
    'company_address',
    'logo_company')

    list_per_page = 5

admin.site.register(Data_Company, tb_data_company)

# -------------------------------------------

class tb_data_template(admin.ModelAdmin):
    list_display = ( 
    'id_template',
    'nama_template',
    'body_template',
    'view_template')

    list_display_link = ( 
    'id_template',
    'nama_template',
    'body_template',
    'view_template')

    list_per_page = 5

admin.site.register(Data_Template, tb_data_template)



 