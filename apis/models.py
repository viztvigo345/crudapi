from django.db import models

# Create your models here.

class Data_MOM(models.Model):
    id_data = models.AutoField(primary_key=True)
    judul_data = models.CharField(max_length=100)
    meeting_title = models.CharField(max_length=255, null=True, blank=True)
    date_time = models.CharField(max_length=100, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    created_on = models.CharField(max_length=100)
    edited_last = models.CharField(max_length=100)
    deleted_on = models.CharField(max_length=100, null=True, blank=True)
    status_document = models.CharField(max_length=3)
    no_template = models.CharField(max_length=2, null=True, blank=True)

class Data_Company(models.Model):
    id_company = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length=100)
    company_address = models.CharField(max_length=255)
    logo_company = models.CharField(max_length=200)

class Data_Template(models.Model):
    id_template = models.AutoField(primary_key=True)
    nama_template = models.CharField(max_length=100)
    body_template = models.TextField(null=True, blank=True)
    view_template = models.CharField(max_length=200)


