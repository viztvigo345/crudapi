from ..models import Data_MOM, Data_Company, Data_Template
from .serializers import Data_MOM_Serializers, Data_Company_Serializers, Data_Template_Serializers
from rest_framework import viewsets, generics

class Data_MOM_Viewset(viewsets.ModelViewSet):
    serializer_class = Data_MOM_Serializers
    queryset = Data_MOM.objects.all()

class Data_MOM_Deleted_Viewset(viewsets.ModelViewSet):
    serializer_class = Data_MOM_Serializers
    queryset = Data_MOM.objects.filter(deleted_on = 0)

class Data_Company_Viewset(viewsets.ModelViewSet):
    serializer_class = Data_Company_Serializers
    queryset = Data_Company.objects.all()

class Data_Template_Viewset(viewsets.ModelViewSet):
    serializer_class = Data_Template_Serializers
    queryset = Data_Template.objects.all()
    

