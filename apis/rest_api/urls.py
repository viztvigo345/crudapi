from .views import Data_MOM_Viewset, Data_MOM_Deleted_Viewset, Data_Template_Viewset, Data_Company_Viewset
from rest_framework.routers import DefaultRouter
from ..views import RegisterAPI, LoginAPI
from django.urls import path, include
from knox import views as knox_views

router = DefaultRouter()
router.register(r'data_mom', Data_MOM_Viewset, basename='data_mom')
router.register(r'data_mom_by_delete', Data_MOM_Deleted_Viewset, basename='data_mom_delete')
router.register(r'data_template', Data_Template_Viewset, basename='data_template')
router.register(r'data_company', Data_Company_Viewset, basename='data_company')

urlpatterns = [
    path('data/', include(router.urls)),
    path('register/', RegisterAPI.as_view(), name='register'),
    path('login/', LoginAPI.as_view(), name='login'),
    path('logout/', knox_views.LogoutView.as_view(), name='logout'),
]
