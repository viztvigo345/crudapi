from rest_framework import serializers
from ..models import Data_MOM, Data_Company, Data_Template
from django.contrib.auth.models import User

class Data_MOM_Serializers(serializers.ModelSerializer):
    class Meta:
        model = Data_MOM
        fields = ('id_data', 
                 'judul_data',
                 'meeting_title', 
                 'date_time', 
                 'body',
                 'created_on', 
                 'edited_last', 
                 'deleted_on', 
                 'status_document',
                 'no_template')

class Data_Company_Serializers(serializers.ModelSerializer):
    class Meta:
        model = Data_Company
        fields = ('id_company',
                  'company_name',
                  'company_address',
                  'logo_company')

class Data_Template_Serializers(serializers.ModelSerializer):
    class Meta:
        model = Data_Template
        fields = ('id_template',
                  'nama_template',
                  'body_template',
                  'view_template')


# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

        return user